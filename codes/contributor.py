# contributor.py
# user-2: additional features will be added to pythonnic.py by the contributor

import sys
import csv
from abc import ABC, abstractmethod

from pythonic import Ring


# Abstract class for table-format
class TableFormat(object):
    """ Abastract class """

    # print data to file or screen
    def __init__(self, out_file=None):
        if out_file == None:
            # stdout is the location where python prints the output
            out_file = sys.stdout
        self.out_file = out_file

    @abstractmethod
    def heading(self, header): # must be implemented in child class
        pass 

    @abstractmethod
    def row(self, row_data): # must be implemented in child class
        pass


# text format for table
class TextFormat(TableFormat):
    """ Text format for table """
    
    # option for modifying width
    def __init__(self, width=7, out_file=None): # override init
        # inherit parent init as well to save data in file
        super().__init__(out_file) 
        self.width = width 

    def heading(self, header): # print headers
        for h in header:
            print("{0:>{1}s}".format(h, self.width), 
                    end=' ', file=self.out_file)
        print(file=self.out_file)

    def row(self, row_data): # print rows
        for r in row_data:
            print("{0:>{1}s}".format(r, self.width), 
                    end=' ', file=self.out_file)
        print(file=self.out_file)


# csv format for table
class CSVFormat(TableFormat):
    """ Text format for table """

    # init will be inherited from parent to save data in file

    def heading(self, header): # print headers
        print(','.join(header), file=self.out_file)

    def row(self, row_data): # print rows
        print(",".join(row_data), file=self.out_file)


# Put quotes around data : This class will be used with other class in Mixin
class QuoteData(object):
    def row(self, row_data):
        quoted_data = ['"{0}"'.format(r) for r in row_data]
        super().row(quoted_data) # nature of super() is decided by child class

# Mixin : QuoteData and CSVFormat
# this will decide the nature of super() in QuoteData
class MixinQuoteCSV(QuoteData, CSVFormat): 
    pass

# Mixin : QuoteData and TextFormat
class MixinQuoteText(QuoteData, TextFormat): 
    pass


# this code is copied from datamine.py file and modified slightly
# to include the type of the data
def read_file(filename, types, mode='warn'):
    ''' read csv file and save data in the list '''

    # check for correct mode
    if mode not in ['warn', 'silent', 'stop']:
        raise ValueError("possible modes are 'warn', 'silent', 'stop'")

    ring_data = [] # create empty list to save data

    with open (filename, 'r') as f:
        rows =  csv.reader(f)
        header = next(rows) # skip the header

        # change the types of the columns
        for row in rows:
            try:
                # row[2] = float(row[2]) # radius
                # row[3] = float(row[3]) # price
                # row[4] = int(row[4]) # quantity

                # generalized conversion
                row = [d_type(val) for d_type, val in zip(types, row)]
            except ValueError as err: # process value error only
                if mode == 'warn':
                    print("Invalid data, row is skipped")
                    print('Row: {}, Reason : {}'.format(row_num, err))
                elif mode == 'silent':
                    pass # do nothing
                elif mode == 'stop':
                    raise # raise the exception
                continue
        
            # ring_data.append(tuple(row)) 

            # append data in list in the form of tuple 
            # row_dict = {
                    # 'date' : row[0],
                    # 'metal' : row[1],
                    # 'radius' : row[2],
                    # 'price' : row[3],
                    # 'quantity' : row[4]
                # }
        
            # row_dict = Ring(row[0], row[1], row[2], row[3], row[4])
            # # use below or above line
            row_dict = Ring(
                    date = row[0],
                    metal = row[1], 
                    radius = row[2], 
                    price = row[3], 
                    quantity = row[4]
                )

            ring_data.append(row_dict) 

    return ring_data


# table formatter
def print_table(list_name, col_name=['metal', 'radius'],
        out_format=TextFormat(out_file=None)): # note that class is passed here 
    """ print the formatted output """

    # for c in col_name: # print header
        # print("{:>7s}".format(c), end=' ')
    # print() # print empty line
    # for l in list_name: # print values
        # for c in col_name:
            # print("{:>7s}".format(str(getattr(l, c))), end=' ')
        # print()
    
    # invoke class-method for printing heading
    out_format.heading(col_name) # class is passed to out_format 
    for l in list_name:
        # store row in a list
        row_data = [str(getattr(l, c)) for c in col_name] 
        out_format.row(row_data) # pass rows to class-method row()


def main():
    # list correct types of columns in csv file
    types =[str, str, float, float, int]

    # read file and save data in list
    list_data = read_file('price.csv', types)

    # # formatted output
    # print_table(list_data)
    # print()
    # print_table(list_data, ['metal', 'radius', 'price'],
            # out_format=TextFormat())

    # print in file
    # out_file = open("text_format.txt", "w") # open file in write mode
    # print_table(list_data, ['metal', 'radius', 'price'], 
            # out_format=TextFormat(out_file=out_file))
    # out_file.close()

    # print in file with width = 15
    # out_file = open("wide_text_format.txt", "w") # open file in write mode
    # print_table(list_data, ['metal', 'radius', 'price'],
            # out_format=TextFormat(width=15, out_file=out_file))
    # out_file.close()

    # print()
    # print_table(list_data, ['metal', 'radius', 'price'], 
            # out_format=CSVFormat())
    # # print in file
    # out_file = open("csv_format.csv", "w") # open file in write mode
    # print_table(list_data, ['metal', 'radius', 'price'], 
            # out_format=CSVFormat(out_file=out_file))
    # out_file.close()

    
    print_table(list_data, ['metal', 'radius', 'price'], 
            out_format=MixinQuoteText())
    
    print() 
    print_table(list_data, ['metal', 'radius', 'price'], 
            out_format=MixinQuoteCSV())

if __name__ == '__main__':
    main()
