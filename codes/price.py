# price.py

import csv

# warn is kept as default, as error should not be passed silently
def ring_cost(filename, mode='warn'):
    ''' calculate the total cost '''

    total_price = 0 # for all items in the list

    with open(filename, 'r') as f: # open file in read mode
        rows = csv.reader(f)
        header = next(rows) # skip line 1 i.e. header
        for row_num, row in enumerate(rows, start=1): # start from 1, not 0)
            try:
                row[3] = float(row[3]) # price
                row[4] = int(row[4]) # quantity
            except ValueError as err: # process ValueError only
                if mode == 'warn': 
                    print("Invalid data, row is skipped")
                    print('Row: {}, Reason : {}'.format(row_num, err))
                elif mode == 'silent':
                    pass # do nothing
                elif mode == 'stop':
                    raise # raise the exception
                continue
            total_price += row[3] * row[4]

    # print("Total price = %10.2f" % total_price)
    return total_price  # return total_price

def main():
    total = ring_cost('price.csv')  # function call
    print("Total price = %10.2f" % total) # print value

# standard boilerplate
# main is the starting function
if __name__ == '__main__':
    main()
